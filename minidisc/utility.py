import errno
import logging
from os import environ
from pathlib import Path
from shutil import which
from typing import Union

ATRACDENC_AVAIL: bool = False


def locate_netmdcli() -> Path:
    """
    Tries to locate a copy of 'netmdcli' by environment variable, PATH,
    and current working directory (in that order).

    This application is core to overall functionality by this manager, so
    not being able to locate it is fatal.

    :return: Path to 'netmdcli'
    :raises: SystemExit
    """
    # Take from explicit environment var
    env_netmdcli = environ.get("NETMDCLI")
    if env_netmdcli is not None:
        logging.debug(f"'netmdcli' specified at '{env_netmdcli}'")
        return Path(env_netmdcli)

    # Attempt to locate in PATH
    whc_netmdcli = which("netmdcli")
    if whc_netmdcli is not None:
        logging.debug(f"'netmdcli' located at '{whc_netmdcli}'")
        return Path(whc_netmdcli)

    # Look in current working dir
    cur_netmdcli = Path.cwd() / "netmdcli"
    if cur_netmdcli.exists():
        logging.debug(f"Using 'netmdcli' in local directory: '{cur_netmdcli}'")
        return cur_netmdcli

    # Return error & quit
    message = "Unable to locate 'netmdcli', please set NETMDCLI environment variable."
    logging.error(msg=message)
    raise SystemExit(errno.ENOENT)


def locate_atracdenc() -> Union[Path, None]:
    """
    Tries to locate a copy of 'atracdenc' (https://github.com/dcherednik/atracdenc)
    by environment variable, PATH, and current working directory (in that order).
    :return: Path to 'atracdenc'
    """
    global ATRACDENC_AVAIL

    # Take from explicit environment var
    env_atracdenc = environ.get("ATRACDENC")
    if env_atracdenc is not None:
        ATRACDENC_AVAIL = True
        logging.debug(f"'netmdcli' specified at '{env_atracdenc}'")
        return Path(env_atracdenc)

    # Attempt to locate in PATH
    whc_atracdenc = which("atracdenc")
    if whc_atracdenc is not None:
        ATRACDENC_AVAIL = True
        logging.debug(f"'netmdcli' located at '{whc_atracdenc}'")
        return Path(whc_atracdenc)

    # Look in current working dir
    cur_atracdenc = Path.cwd() / "atracdenc"
    if cur_atracdenc.exists():
        ATRACDENC_AVAIL = True
        logging.debug(f"Using 'netmdcli' in local directory: '{cur_atracdenc}'")
        return cur_atracdenc

    # Return warning message
    logging.warning(msg="Unable to locate 'atracdenc'; ATRAC encoding will be unavailable. ")
    return None

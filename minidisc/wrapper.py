import logging
import subprocess
from pathlib import Path
from shlex import split as ssplit
from tempfile import TemporaryDirectory
from typing import Union, List
from time import sleep
import re

import ffmpeg  # type: ignore

import minidisc.utility as util

NETMDCLI_PATH = util.locate_netmdcli()
ATRACDENC_PATH = util.locate_atracdenc()
TEMPDIR = TemporaryDirectory(prefix="minimanager-")

if util.ATRACDENC_AVAIL:
    formats = ["pcm_s16le", "wav", "atrac3", "atrac3_lp4"]
else:
    formats = ["pcm_s16le", "wav"]


def transcode(infile: Union[Path, str], enctype: str = "wav", speed: float = 1.0) -> Path:
    """
    Transcodes file using ffmpeg to the needed format, WAV (or ATRAC3/3LP4 if
    'atracdenc' available). The new file will be placed in a temporary
    directory and cleaned up on exit.

    :param infile: The input file
    :param enctype: The encoding type
    :param speed: The tempo to transcode to (1.0 == regular speed); useful for podcasts
    :return:
    """
    if enctype not in formats:
        raise ValueError(f"Unexpected encoding type: expected one of {formats}, got '{enctype}'.")

    if not isinstance(infile, Path):
        infile = Path(infile)

    ###########################################################################
    # WAV Encoding
    ###########################################################################
    outfile = Path(TEMPDIR.name) / f"{infile.stem}.wav"
    logging.info(f"Transcoding {infile.name} -> {str(outfile)}")
    try:
        fout, ferr = (
            ffmpeg
            .input(str(infile))
            .audio.filter("atempo", speed)
            .output(str(outfile))
            .run(capture_stdout=True, capture_stderr=True, overwrite_output=True)
        )
    except ffmpeg.Error as transcode_error:
        logging.error(f"Error transcoding file:\n {transcode_error.stderr.decode('utf-8')}")
        raise ChildProcessError(transcode_error.stderr.decode('utf-8'))

    # Display verbose debug information on successful transcode
    if fout is not None and fout != "":
        logging.debug(fout)

    if ferr is not None and ferr != "":
        logging.debug(ferr)

    ###########################################################################
    # ATRAC3 Encoding
    #
    # If possible, transcode to ATRAC3 and then wrap file with WAV container
    ###########################################################################
    if util.ATRACDENC_AVAIL and (enctype == "atrac3" or enctype == "atrac3_lp4"):
        # Reassign WAV file to own variable; _update outfile to be ATRAC
        wavfile = outfile
        outfile = Path(TEMPDIR.name) / f"{infile.stem}.{enctype}"

        # Encode to ATRAC
        logging.info(f"Transcoding {wavfile.name} -> {str(outfile)}")
        args = f"{str(ATRACDENC_PATH)} -e {enctype} -i \"{str(wavfile)}\" -o \"{str(outfile)}\""
        cmd = subprocess.run(ssplit(args), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        if cmd.returncode != 0:
            logging.error(f"Error converting to {enctype.upper()}:\n{cmd.stdout.decode('utf-8')}")
            return None

        # Wrap ATRAC in WAV/RIFF container
        atracfile = outfile
        outfile = Path(TEMPDIR.name) / f"{atracfile.stem}.{enctype}.wav"
        logging.info(f"Wrapping {atracfile.name} in WAV/RIFF container")
        try:
            fout, ferr = (
                ffmpeg
                .input(str(atracfile))
                .output(str(outfile), acodec="copy")
                .run(capture_stdout=True, capture_stderr=True, overwrite_output=True)
            )
        except ffmpeg.Error as transcode_error:
            logging.error(f"Error transcoding file:\n {transcode_error.stderr.decode('utf-8')}")
            raise ChildProcessError(transcode_error.stderr.decode('utf-8'))

        # Display verbose debug information on successful transcode
        if fout is not None and fout != "":
            logging.debug(fout)

        if ferr is not None and ferr != "":
            logging.debug(ferr)

    # Return whichever file was transcoded (WAV or ATRAC)
    return outfile


class MDTrack:
    """
    Class representing a single track on the MiniDisc,
    including it's associated info and metadata.
    """
    def __init__(self,
                 title: str = "",
                 protected: bool = False,
                 protection_type: str = "",
                 length: float = 0) -> None:
        """
        :param title: The track title
        :param protected: Whether the track is protected, e.g. on commercial release
        :param protection_type: What type of protection is used on the track
        :param length: Length of the track (may not be accurate for LP2/LP4 ATRAC3;
            tracks that are encoded use less available space on the disc)
        """
        self.title: str = title
        self.protected: bool = protected
        self.protection_type: str = protection_type
        self.length: float = length

    def __repr__(self):
        if self.title is None or self.title == "":
            return "Track: (No Title)"
        else:
            return f"Track: {self.title}"


class NetMD:
    """
    Class representing a NetMD disc, loaded in the device
    """
    def __init__(self) -> None:
        self.title: str = "No Title"
        # Time used
        self.time_used: float = 0
        self.time_total: float = 0
        self.time_remaining: float = 0
        self.percent_full: float = 0
        self.tracks: list = []

        # Attempt to gather disc information
        self._update()

    def __repr__(self):
        return f"MiniDisc: {self.title}"

    def _update(self) -> None:
        """Utility class to _update all disc info"""
        self.get_info()
        self.get_tracks()

    # TODO: Implement the following from `netmdcli`
    #
    # GROUP:
    #     newgroup <string> - create a new group named <string>
    #     retitle #1 <string> - rename group number #1 to <string>
    #     deletegroup #1 - delete a group, but not the tracks in it
    #     group #1 #2 - Stick track #1 into group #2
    #
    # MISC:
    #     m3uimport <file> - import song and disc title from a playlist
    def get_info(self) -> None:
        """
        Calls 'netmdcli capacity' to get disc information
        """
        logging.info("Refreshing disc info...")
        args = f"{str(NETMDCLI_PATH)} capacity"
        cmd = subprocess.run(ssplit(args), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        if cmd.returncode != 0:
            logging.error(f"Error getting capacity:\n{cmd.stdout.decode('utf-8')}")
            return None
        else:
            # Carve up the output to get needed disc capacity information
            output = cmd.stdout.decode("utf-8").split("\n")
            for line in output:
                secs: float = 0.0
                if line.startswith("Disc Title"):
                    self.title = line.split("Disc Title: ")[1]

                if line.startswith("Recorded"):
                    rec_line = line.replace(" ", "").split(":")
                    secs += ((int(rec_line[1]) * 60) * 60) + (int(rec_line[2]) * 60) + float(rec_line[3])
                    self.time_used = secs
                elif line.startswith("Total"):
                    tot_line = line.replace(" ", "").split(":")
                    secs += ((int(tot_line[1]) * 60) * 60) + (int(tot_line[2]) * 60) + float(tot_line[3])
                    self.time_total = secs
                elif line.startswith("Available"):
                    rem_line = line.replace(" ", "").split(":")
                    secs += ((int(rem_line[1]) * 60) * 60) + (int(rem_line[2]) * 60) + float(rem_line[3])
                    self.time_remaining = secs

            self.percent_full = round(100 - ((self.time_remaining / self.time_total) * 100), 2)

    def get_tracks(self) -> None:
        """
        Base 'netmdcli' command - list minidisc contents
        """
        logging.info("Refreshing tracklist...")
        tracks = []
        cmd = subprocess.run(ssplit(str(NETMDCLI_PATH)), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        if cmd.returncode != 0:
            logging.error(f"Error listing minidisc:\n{cmd.stdout.decode('utf-8')}")
        else:
            for line in cmd.stdout.decode("utf-8").split("\n"):
                if line.startswith("Track"):
                    track = self._parse_track(line)
                    tracks.append(track)

            self.tracks = tracks

    def rename_track(self, tracklist_index: int, new_title: str, update_tracklist: bool = True) -> None:
        """
        Rename or add a track title: 'netmdcli rename'

        :param tracklist_index: The track listing index to rename. Note that this is not the same
            as the track number; tracks are off-by-one (e.g. track 1 is index 0).
        :param new_title: The new track name.
        :param update_tracklist: Whether to automatically call self.get_tracks() after _update; if doing
            batch renaming, set to False, and manually call _update after the batch rename.
        :returns: None
        """

        # Make sure that the given index isn't longer than the tracklist;
        # wrapper doesn't directly reference the index, no exception to catch.
        if 0 <= tracklist_index <= len(self.tracks)-1:
            logging.info(f"Renaming track {tracklist_index + 1} (index {tracklist_index}: "
                         f"\"{self.tracks[tracklist_index].title}\" -> \"{new_title}\"")

            # Build the arguments and rename the track.
            args = f"{str(NETMDCLI_PATH)} rename {tracklist_index} \"{new_title}\""
            cmd = subprocess.run(ssplit(args), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            if cmd.returncode != 0:
                logging.error(f"Error renaming track:\n{cmd.stdout.decode('utf-8')}")
            else:
                # Refresh the tracks if the flag is set
                if update_tracklist:
                    self.get_tracks()
        else:
            logging.error(f"No such track index {tracklist_index}; {len(self.tracks)} total tracks "
                          f"(0-{len(self.tracks)-1})")

    def move_track(self, index_old: int, index_new: int, update_tracklist: bool = True) -> None:
        """
        Move track from index position #1 to index position #2. 'netmdcli move'

        :param index_old: The original track index position
        :param index_new: The new track index position
        :param update_tracklist: Whether to automatically call self.get_tracks() after _update; if doing
            batch renaming, set to False, and manually call _update after the batch rename.
        :return: None
        """
        if index_new == index_old:
            logging.info("Not moving tracks; source and destination are the same")
        elif len(self.tracks)-1 < 0:
            logging.warning("There are no tracks on the current disc.")
        else:
            if 0 <= index_old <= len(self.tracks)-1 and 0 <= index_new <= len(self.tracks)-1:
                logging.info(f"Moving track index {index_old} -> {index_new}...")
                args = f"{str(NETMDCLI_PATH)} move {index_old} {index_new}"
                cmd = subprocess.run(ssplit(args), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                if cmd.returncode != 0:
                    logging.error(f"Error moving track:\n{cmd.stdout.decode('utf-8')}")
                else:
                    if update_tracklist:
                        self.get_tracks()
            else:
                logging.error(f"Invalid index provided; must be 0-{len(self.tracks)-1}")

    def delete_track(self, track_index: int, update_tracklist: bool = True) -> None:
        """
        Deletes a track from the tracklist index. 'netmdcli delete'

        :param track_index: The track index to delete
        :param update_tracklist: Whether to automatically call self.get_tracks() after _update; if doing
            batch renaming, set to False, and manually call _update after the batch rename.
        :return: None
        """
        if 0 <= track_index <= len(self.tracks)-1:
            logging.info(f"Deleting track index {track_index}...")
            args = f"{str(NETMDCLI_PATH)} delete {track_index}"
            cmd = subprocess.run(ssplit(args), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            if cmd.returncode != 0:
                logging.error(f"Error deleting track:\n{cmd.stdout.decode('utf-8')}")
            else:
                if update_tracklist:
                    self.get_tracks()
        elif len(self.tracks)-1 < 0:
            logging.warning("There are no tracks on the current disc.")
        else:
            logging.error(f"Invalid index provided; must be 0-{len(self.tracks) - 1}")

    def delete_tracks(self, track_index_start: int, track_index_end: int) -> None:
        """
        Deletes a range of tracks, inclusive.

        :param track_index_start: The track index to start deleting from
        :param track_index_end: The final track index end to delete
        :return: None
        """
        if 0 <= track_index_start <= len(self.tracks) - 1 and 0 <= track_index_end <= len(self.tracks) and \
                track_index_start <= track_index_end:
            for idx in range(track_index_start, track_index_end + 1):
                self.delete_track(idx, update_tracklist=False)

                # For whatever reason, spamming delete at the device sometimes doesn't fully delete
                # a track.
                sleep(.5)
            self._update()
        else:
            logging.error(f"Invalid index provided; must be 0-{len(self.tracks) - 1}")

    def set_title(self, disc_title: str) -> None:
        args = f"{str(NETMDCLI_PATH)} settitle \"{disc_title}\""

        logging.info(f"Setting disc title: \"{disc_title}\"")
        cmd = subprocess.run(ssplit(args), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        if cmd.returncode != 0:
            logging.error(f"Error setting disc title:\n{cmd.stdout.decode('utf-8')}")
        else:
            self._update()

    def transfer_track(self, filename: Union[Path, str],
                       title: Union[None, str] = None,
                       enctype: str = "wav",
                       speed: float = 1.0,
                       update_tracklist: bool = True,
                       force_transcode: bool = False) -> None:
        """
        ('netmdcli send')
        Sends a file to the NetMD player. If the file is not in the correct format, it will be transcoded on the fly.

            Supported file formats: 16 bit pcm (stereo or mono) @44100Hz or
            Atrac LP2/LP4 data stored in a WAV container.

        :param filename: The path to the file (Path() or string)
        :param title: Optional title; if not specified, it will default to the filename
        :param enctype: Which format to transcode to if needed, WAV or (optionally) ATRAC1 [if 'atracdenc' is available]
        :param update_tracklist: Whether to automatically call self.get_tracks() after _update; if doing
            batch renaming, set to False, and manually call _update after the batch rename.
        :param speed: If encoding, the speed at which the audio will be played back (for podcasts, books, etc.)
        :param force_transcode: Whether or not to force transcoding, ignoring the results of the file probe
        :return: None
        """
        # Probe the file to see if we need to transcode it
        try:
            if self._probe_audio_file(str(filename)) or force_transcode:
                # Try to transcode the file; log error and return if failure
                try:
                    xfer_file = str(transcode(Path(filename), enctype, speed=speed))
                except ChildProcessError:
                    logging.error("Unable to transcode file, not transferring.")
                    return None
            else:
                xfer_file = str(filename)

            args = f"{str(NETMDCLI_PATH)} send \"{xfer_file}\""

            # Append the title to the args; set to the filename if not provided
            if title is not None:
                file_title = title
                args += f" \"{file_title}\""
            else:
                file_title = Path(xfer_file).name.rstrip(''.join(Path(xfer_file).suffixes))
                args += f" \"{file_title}\""

            logging.info(f"Sending {file_title} -> Minidisc")
            cmd = subprocess.run(ssplit(args), stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            if cmd.returncode != 0:
                logging.error(f"Error sending track:\n{cmd.stdout.decode('utf-8')}")
            else:
                if update_tracklist:
                    self.get_tracks()

        except FileNotFoundError:
            logging.error("File {str(filename)} not found; not transferring.")

    def transfer_tracks(self, *tracks,
                        enctype: str = "wav",
                        speed: float = 1.0,
                        update_tracklist: bool = False,
                        force_transcode: bool = False) -> None:
        """
        Transfers multiple tracks to the device

        :param tracks: A collection of tracks, e.g. filesystem glob or multiple filepaths
        :param enctype: The encoding type of the track
        :param update_tracklist: Whether to update the tracklist after each transfer
        :param speed: If encoding, the speed at which the audio will be played back (for podcasts, books, etc.)
        :param force_transcode: Whether or not to force transcoding, ignoring the results of the file probe
        :return: None
        """
        total_tracks = len(tracks)
        current_track = 1
        for track in tracks:
            # Coerce the filepath into a Path object
            if not isinstance(track, Path):
                track = Path(track)

            logging.info(f"Transferring track {current_track} of {total_tracks}")
            self.transfer_track(track, enctype=enctype, update_tracklist=update_tracklist,
                                speed=speed, force_transcode=force_transcode)
            current_track += 1

        # Do a final tracklist update
        self._update()

    @staticmethod
    def _probe_audio_file(filename: Union[str, Path]) -> bool:
        """
        Probe a given audio file to determine if it needs to be transcoded before sending to the NetMD.
        :param filename:
        :return: Boolean
        """
        needs_transcode: bool = False

        # Gather file details
        try:
            fdetails = ffmpeg.probe(str(filename))
            for stream in fdetails["streams"]:
                if stream["codec_type"] == "audio":
                    codec_name = stream["codec_name"]
                    sample_rate = stream["sample_rate"]
                    channels = stream["channels"]
                    if codec_name not in formats:
                        logging.debug(f"Incorrect audio codec: {codec_name}; attempt to transcode")
                        needs_transcode = True
                    elif sample_rate != "44100":
                        logging.debug(f"Incorrect audio sample rate: {sample_rate}; attempt to transcode")
                        needs_transcode = True
                    elif channels > 2:
                        logging.debug(f"Incorrect channel count: {channels}; attempt to transcode")
                        needs_transcode = True
        except ffmpeg.Error as probe_error:
            logging.error(f"Unable to probe file: {probe_error.stderr.decode('utf-8')}")
            raise

        return needs_transcode

    @staticmethod
    def _parse_track(line) -> Union[MDTrack, None]:
        if line.startswith("Track"):
            track_reg = r"^Track\s+\d+:\s(.*)\s\-\s(\d+\:\d+\:\d+)\s\-\s(.*$)"

            # Match the track line, and parse out information
            track_info = re.split(track_reg, line)

            # Split the protection into protection bool and type
            # noinspection PyTypeChecker
            track_info[1] = [i for i in track_info[1].split(" ") if i != " "]

            # Check if the track is listed as protected or not
            is_protected: bool = False
            if track_info[1][0] != "UnPROT":
                is_protected = True

            # Parse the track info
            track_len_str: List[str] = track_info[2].split(":")
            track_len: int = (int(track_len_str[0]) * 60) + int(track_len_str[1])

            track = MDTrack(
                protected=is_protected,
                protection_type=track_info[1][1],
                title=track_info[3],
                length=track_len
            )

            return track
        return None

import logging
from os import environ
from pathlib import Path
from unittest import TestCase
from unittest.mock import patch

from minidisc.utility import locate_netmdcli, locate_atracdenc


class TestNETMDCLI(TestCase):
    def test_locatenmdcli_env(self) -> None:
        """Test explicit environment variable setting."""
        environ["NETMDCLI"] = "/fake/path/netmdcli"
        nmdclipath = locate_netmdcli()

        self.assertEqual(nmdclipath, Path("/fake/path/netmdcli"))

    @patch("backend.minidisc.utility.which")
    def test_locatenmdcli_path(self, m_which) -> None:
        """Test PATH location."""
        m_which.return_value = "/fake/path/netmdcli"

        nmdclipath = locate_netmdcli()

        self.assertEqual(nmdclipath, Path("/fake/path/netmdcli"))

    @patch("backend.minidisc.utility.which")
    @patch("backend.minidisc.utility.Path.exists")
    def test_locatenmdcli_current(self, m_exists, m_which) -> None:
        """Test 'current' location."""
        m_which.return_value = None
        m_exists.return_value = True

        nmdclipath = locate_netmdcli()

        self.assertEqual(nmdclipath, Path(Path.cwd() / "netmdcli"))

    @patch("backend.minidisc.utility.which")
    @patch("backend.minidisc.utility.Path.exists")
    def test_locatenmdcli_fail(self, m_exists, m_which) -> None:
        """Test that failure to locate netmdcli results in system exit."""
        m_which.return_value = None
        m_exists.return_value = False

        self.assertRaises(SystemExit, locate_netmdcli)

    def tearDown(self) -> None:
        # Remove custom env
        if environ.get("NETMDCLI"):
            del environ["NETMDCLI"]


class TestATRACDENC(TestCase):
    def test_locateatracdenc_env(self) -> None:
        """Test explicit environment variable setting."""
        environ["ATRACDENC"] = "/fake/path/atracdenc"
        atracdencpath = locate_atracdenc()

        self.assertEqual(atracdencpath, Path("/fake/path/atracdenc"))

    @patch("backend.minidisc.utility.which")
    def test_locateatracdenc_path(self, m_which) -> None:
        """Test PATH location."""
        m_which.return_value = "/fake/path/atracdenc"

        atracdencpath = locate_atracdenc()

        self.assertEqual(atracdencpath, Path("/fake/path/atracdenc"))

    @patch("backend.minidisc.utility.which")
    @patch("backend.minidisc.utility.Path.exists")
    def test_locateatracdenc_current(self, m_exists, m_which) -> None:
        """Test 'current' location."""
        m_which.return_value = None
        m_exists.return_value = True

        atracdencpath = locate_atracdenc()

        self.assertEqual(atracdencpath, Path(Path.cwd() / "atracdenc"))

    @patch("backend.minidisc.utility.which")
    @patch("backend.minidisc.utility.Path.exists")
    def test_locateatracdenc_fail(self, m_exists, m_which) -> None:
        """Test that failure to locate atracdenc results warning log."""
        m_which.return_value = None
        m_exists.return_value = False

        with self.assertLogs(level=logging.DEBUG):
            self.assertIsNone(locate_atracdenc())

    def tearDown(self) -> None:
        # Remove custom env
        if environ.get("ATRACDENC"):
            del environ["ATRACDENC"]
